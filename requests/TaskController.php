<?php

class TaskController
{

    private $accessToken;
    private $url = 'http://ec2-52-59-205-209.eu-central-1.compute.amazonaws.com/api/v0.1/';

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }


    function getTasks()
    {
        $ch = curl_init();

        $headers = [
            'authorization: Bearer ' . $_SESSION['token']['accessToken']
        ];

        curl_setopt($ch, CURLOPT_URL, $this->url . 'task');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $resp = curl_exec($ch);

        if ($e = curl_error($ch)) {
            echo $e;
            die();
        } else {
            $decoded = json_decode($resp, true);
            if (isset($decoded['error'])) {
                pre_r($decoded);
                throw new Exception('Error: ' . $decoded['error']['description']);
            }
        }
        curl_close($ch);

        return $decoded;
    }

    function getTasksByListId($listId)
    {
        $filteredTasks = [];

        foreach ($this->getTasks()['data']['rows'] as $row) {
            if ($row['listId'] == $listId) {
                $filteredTasks[] = $row;
            }
        }

        return $filteredTasks;
    }
}