<?php

require 'utils.php';

class LoginController {
    private $url = 'http://ec2-52-59-205-209.eu-central-1.compute.amazonaws.com/api/v0.1/';
    private $code;

    function login($username, $password): void {

        $ch = curl_init();

        $data_array = array(
            'username' => $username,
            'password' => $password
        );

        $headers = [
            'content-type: application/json',
            'user-agent: hi'
        ];

        curl_setopt($ch, CURLOPT_URL, $this->url . '/authentication/login');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $resp = curl_exec($ch);

        if($e = curl_error($ch)) {
            echo $e;
            die();
        } else {
            $decoded = json_decode($resp, true);
            if (isset($decoded['error'])) {
                pre_r($decoded);
                throw new Exception('Failed to log in: ' . $decoded['error']['description']);
            }

            $this->code = $decoded['code'];
        }
        curl_close($ch);
    }

    function getNewToken() {
        if (!isset($this->code)) {
            throw new Exception('"code" property isn\'t set' );
        }

        $url = 'http://ec2-52-59-205-209.eu-central-1.compute.amazonaws.com/api/v0.1/token';
        $ch = curl_init();

        $data_array = [
            "grantType" => "authorization_code",
            "code" => $this->code,
            "refreshToken" => "refreshToken"
        ];
        $data = http_build_query($data_array);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)) {
            echo $e;
            echo 'goodbye from LoginController.php';
            die();
        } else {
            curl_close($ch);
            $decoded = json_decode($resp, true);
            if (isset($decoded['error'])) {
                pre_r($decoded);
                throw new Exception('Failed to get access token' . $decoded['error']['description']);
            }
            return $decoded;
        }
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


}