<?php

function pre_r($str, $isReturn = false) {
    $text = '<pre>' . print_r($str, true) . '</pre>';
    if ($isReturn) {
        return $text;
    } else {
        echo $text;
    }
}