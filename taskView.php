<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>test index page</title>
    <style>
        h2 {
            text-align: center;
        }
        h1 {
            text-align: center;
            padding-top: 100px;
            margin-bottom: 60px;
        }
        .taskItem {
            text-align: center;
            width: 300px;
            margin: 20px auto;
            border: 3px solid black;
            padding: 20px;
        }

        .taskItem:hover {
            background-color: #74ff9b;
        }
    </style>
</head>
    <body>
        <h1>Tasks view</h1>

            <?php
            session_start();

            if (!isset($_GET['listId'])) {
                echo '<h2 style="color: red">List ID not found :(</h2>';
                return;
            }

            require 'utils.php';
            require 'requests/TaskController.php';

            $taskController = new TaskController($_SESSION['token']['accessToken']);
            $taskData = $taskController->getTasksByListId($_GET['listId']);

            if (empty($taskData)) {
                echo '<h2 style="color: #ff6ec5">No records found</h2>';
                return;
            }

            foreach ($taskData as $row) {
                echo '<div taskId="' . $row['id'] . '" class="taskItem">' . $row['name'] . '</div>';
            }
            ?>
    </body>
</html>