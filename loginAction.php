<?php
session_start();

if (isset($_POST['username']) && isset($_POST['password'])) {

    require 'requests/LoginController.php';

    $loginController = new LoginController();

    try {
        $loginController->login($_POST['username'], $_POST['password']);
        $_SESSION['token'] = $loginController->getNewToken();
        header("Location: listView.php");
        return;
    } catch (Exception $e) {
        header("Location: index.php?error=" . urlencode($e->getMessage()));
        return;
    }
} else {
    header("Location: index.php?error=Some error occured");
    exit();
}