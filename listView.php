<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>test index page</title>
    <style>
        h1 {
            text-align: center;
            padding-top: 100px;
            margin-bottom: 60px;
        }
        .listItem {
            text-align: center;
            width: 300px;
            margin: 20px auto;
            border: 3px solid black;
            padding: 20px;
        }

        .listItem:hover {
            background-color: #74c2ff;
        }
    </style>
</head>
    <body>
        <h1>List view</h1>

            <?php
            require 'utils.php';
            require 'requests/ListController.php';
            session_start();

            $listController = new ListController($_SESSION['token']['accessToken']);
            $listData = $listController->getLists();

            foreach ($listData['data']['rows'] as $row) {
                echo '<a href="taskView.php?listId=' . urlencode($row['id']) . '"><div listId="' .
                    $row['id'] . '" class="listItem">' .
                    $row['name'] . '</div></a>';
            }
            ?>
    </body>
</html>