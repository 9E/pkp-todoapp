<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>test index page</title>
    <style>
        h1, form {
            text-align: center;
        }
        h1 {
            padding-top: 100px;
        }
    </style>
</head>
    <body>
        <h1>Hello, please login</h1>

        <form action="loginAction.php" method="post">
            <?php
                session_start();

                if (isset($_GET['error'])) {
                    echo '<h2 style="color: red">' . $_GET['error'] . '</h2>';
                }
            ?>
            <p>Username: <input type="text" name="username"/></p>
            <p>Password: <input type="password" name="password" /></p>
            <p><input type="submit" value="Login"/></p>
        </form>
    </body>
</html>